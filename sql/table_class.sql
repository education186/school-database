drop table if exists class;

create table if not exists class
(
	id bigserial primary key,
	name varchar not null,
	number int not null
);
