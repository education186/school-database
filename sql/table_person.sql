drop table if exists person;

create table if not exists person
(
	id bigserial primary key,
	first_name varchar not null,
	middle_name varchar,
	last_name varchar not null,
	birth_day date
);
