drop table if exists student;

create table if not exists student
(
	id bigserial primary key,
	id_person bigint not null,
	id_class bigint not null
);	
alter table student add foreign key (id_person) references person (id);
alter table student add foreign key (id_class) references class (id);
